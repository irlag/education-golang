package cmd

import (
	"education-golang/app/config"
	"github.com/golang-migrate/migrate/v4"
	"github.com/spf13/cobra"
)

var migrateCmd = &cobra.Command{
	Use:   "migrations:migrate",
	Short: "Run migrations",
	Long:  "",
	Run: func(cmd *cobra.Command, args []string) {
		dbConfig, err := config.NewDBConfig()
		if err != nil {
			panic(err)
		}

		migration, err := migrate.New("file://migrations", dbConfig.DSN)
		if err != nil {
			panic(err)
		}

		err = migration.Up()
		if err != nil {
			panic(err)
		}
	},
}
