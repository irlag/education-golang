package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var consoleCmd = &cobra.Command{
	Use:   "about",
	Short: "Education Golang project",
	Long: `
	Golang programming language education project
`,
}

func init() {
	consoleCmd.AddCommand(apiServer)
	consoleCmd.AddCommand(migrateCmd)
}

func Execute() {
	if err := consoleCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
