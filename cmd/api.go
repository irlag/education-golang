package cmd

import (
	"log"

	"github.com/spf13/cobra"

	"education-golang/app/config"
	"education-golang/app/server"
)

var apiServer = &cobra.Command{
	Use:   "api-server",
	Short: "Run api http server",
	Long:  "Run api rest http server",
	Run: func(cmd *cobra.Command, args []string) {
		config, err := config.NewConfig()

		if err != nil {
			log.Fatal(err)
		}

		s := server.New(config)

		if err := s.Start(); err != nil {
			log.Fatal(err)
		}
	},
}
