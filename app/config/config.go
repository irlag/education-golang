package config

import (
	"github.com/vrischmann/envconfig"
)

type Config struct {
	BindAddress string `envconfig:"BIND_ADDRESS"`
	Port        string `envconfig:"PORT"`
	DB          *DBConfig
	Debug       bool `envconfig:"DEBUG"`
}

func NewConfig() (*Config, error) {
	config := &Config{
		BindAddress: "0.0.0.0",
		Port:        "8080",
		Debug:       false,
	}

	if err := envconfig.Init(config); err != nil {
		return nil, err
	}

	return config, nil
}
