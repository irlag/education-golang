package config

import (
	"github.com/vrischmann/envconfig"
)

type DBConfig struct {
	DSN    string `envconfig:"DB_DSN"`
	Driver string `envconfig:"DB_DRIVER"`
}

func NewDBConfig() (*DBConfig, error) {
	config := &DBConfig{}

	if err := envconfig.Init(config); err != nil {
		return nil, err
	}

	return config, nil
}
