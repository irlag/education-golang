package server

import (
	"education-golang/app/api"
	"education-golang/app/config"
	appProcessors "education-golang/app/processors"
	db "education-golang/app/storage/db/sqlc"
	"fmt"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"go.uber.org/zap"
	"log"
	"net/http"
)

type Server struct {
	config *config.Config
	Logger *zap.Logger
	Router *mux.Router
}

func New(config *config.Config) *Server {
	logger, err := NewLogger(config.Debug)

	if err != nil {
		log.Fatalf("can't initialize zap logger: %v", err)
	}

	server := &Server{
		config: config,
		Logger: logger,
	}

	server.Router = NewRouter()

	store := db.NewStore()
	err = store.Open(config.DB)
	if err != nil {
		log.Fatal(err)
	}

	//defer store.Close()

	processors := appProcessors.NewProcessor(store, logger)

	api.NewProductsApi(processors).HandleMethods(server.Router)

	return server
}

func (s *Server) Start() error {
	url := fmt.Sprintf("%s:%s", s.config.BindAddress, s.config.Port)

	s.Logger.Info(fmt.Sprintf("starting api server at %s", url))

	return http.ListenAndServe(url, s.Router)
}
