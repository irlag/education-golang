-- name: GetProduct :one
SELECT * FROM products
WHERE id = $1;

-- name: GetProductForUpdate :one
SELECT * FROM products
WHERE id = $1 LIMIT 1
FOR UPDATE;

-- name: ListProduct :many
SELECT * FROM products
ORDER BY id DESC
LIMIT $1
OFFSET $2;

-- name: CreateProduct :one
INSERT INTO products (
    name,
    article,
    brand,
    country_of_origin,
    description,
    price
) VALUES (
     $1, $2, $3, $4, $5, $6
 ) RETURNING *;

-- name: UpdateProduct :one
UPDATE products SET
    name = $2,
    article  = $3,
    brand = $4,
    country_of_origin = $5,
    description = $6,
    price = $7
WHERE id = $1
RETURNING *;

-- name: UpdateProductVote :one
UPDATE products SET
    vote_count = $2,
    vote_sum = $3,
    rating = $4
WHERE id = $1
RETURNING *;

-- name: DeleteProduct :execrows
DELETE FROM products
WHERE id = $1;