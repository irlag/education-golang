// Code generated by sqlc. DO NOT EDIT.
// source: products.sql

package db

import (
	"context"
	"database/sql"
)

const createProduct = `-- name: CreateProduct :one
INSERT INTO products (
    name,
    article,
    brand,
    country_of_origin,
    description,
    price
) VALUES (
     $1, $2, $3, $4, $5, $6
 ) RETURNING id, name, article, brand, country_of_origin, description, price, rating, vote_count, vote_sum
`

type CreateProductParams struct {
	Name            string         `json:"name"`
	Article         string         `json:"article"`
	Brand           string         `json:"brand"`
	CountryOfOrigin string         `json:"country_of_origin"`
	Description     sql.NullString `json:"description"`
	Price           string         `json:"price"`
}

func (q *Queries) CreateProduct(ctx context.Context, arg CreateProductParams) (Product, error) {
	row := q.db.QueryRowContext(ctx, createProduct,
		arg.Name,
		arg.Article,
		arg.Brand,
		arg.CountryOfOrigin,
		arg.Description,
		arg.Price,
	)
	var i Product
	err := row.Scan(
		&i.ID,
		&i.Name,
		&i.Article,
		&i.Brand,
		&i.CountryOfOrigin,
		&i.Description,
		&i.Price,
		&i.Rating,
		&i.VoteCount,
		&i.VoteSum,
	)
	return i, err
}

const deleteProduct = `-- name: DeleteProduct :execrows
DELETE FROM products
WHERE id = $1
`

func (q *Queries) DeleteProduct(ctx context.Context, id int32) (int64, error) {
	result, err := q.db.ExecContext(ctx, deleteProduct, id)
	if err != nil {
		return 0, err
	}
	return result.RowsAffected()
}

const getProduct = `-- name: GetProduct :one
SELECT id, name, article, brand, country_of_origin, description, price, rating, vote_count, vote_sum FROM products
WHERE id = $1 LIMIT 1
`

func (q *Queries) GetProduct(ctx context.Context, id int32) (Product, error) {
	row := q.db.QueryRowContext(ctx, getProduct, id)
	var i Product
	err := row.Scan(
		&i.ID,
		&i.Name,
		&i.Article,
		&i.Brand,
		&i.CountryOfOrigin,
		&i.Description,
		&i.Price,
		&i.Rating,
		&i.VoteCount,
		&i.VoteSum,
	)
	return i, err
}

const getProductForUpdate = `-- name: GetProductForUpdate :one
SELECT id, name, article, brand, country_of_origin, description, price, rating, vote_count, vote_sum FROM products
WHERE id = $1 LIMIT 1
FOR UPDATE
`

func (q *Queries) GetProductForUpdate(ctx context.Context, id int32) (Product, error) {
	row := q.db.QueryRowContext(ctx, getProductForUpdate, id)
	var i Product
	err := row.Scan(
		&i.ID,
		&i.Name,
		&i.Article,
		&i.Brand,
		&i.CountryOfOrigin,
		&i.Description,
		&i.Price,
		&i.Rating,
		&i.VoteCount,
		&i.VoteSum,
	)
	return i, err
}

const listProduct = `-- name: ListProduct :many
SELECT id, name, article, brand, country_of_origin, description, price, rating, vote_count, vote_sum FROM products
ORDER BY id DESC
LIMIT $1
OFFSET $2
`

type ListProductParams struct {
	Limit  int32 `json:"limit"`
	Offset int32 `json:"offset"`
}

func (q *Queries) ListProduct(ctx context.Context, arg ListProductParams) ([]Product, error) {
	rows, err := q.db.QueryContext(ctx, listProduct, arg.Limit, arg.Offset)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	items := []Product{}
	for rows.Next() {
		var i Product
		if err := rows.Scan(
			&i.ID,
			&i.Name,
			&i.Article,
			&i.Brand,
			&i.CountryOfOrigin,
			&i.Description,
			&i.Price,
			&i.Rating,
			&i.VoteCount,
			&i.VoteSum,
		); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

const updateProduct = `-- name: UpdateProduct :one
UPDATE products SET
    name = $2,
    article  = $3,
    brand = $4,
    country_of_origin = $5,
    description = $6,
    price = $7
WHERE id = $1
RETURNING id, name, article, brand, country_of_origin, description, price, rating, vote_count, vote_sum
`

type UpdateProductParams struct {
	ID              int32          `json:"id"`
	Name            string         `json:"name"`
	Article         string         `json:"article"`
	Brand           string         `json:"brand"`
	CountryOfOrigin string         `json:"country_of_origin"`
	Description     sql.NullString `json:"description"`
	Price           string         `json:"price"`
}

func (q *Queries) UpdateProduct(ctx context.Context, arg UpdateProductParams) (Product, error) {
	row := q.db.QueryRowContext(ctx, updateProduct,
		arg.ID,
		arg.Name,
		arg.Article,
		arg.Brand,
		arg.CountryOfOrigin,
		arg.Description,
		arg.Price,
	)
	var i Product
	err := row.Scan(
		&i.ID,
		&i.Name,
		&i.Article,
		&i.Brand,
		&i.CountryOfOrigin,
		&i.Description,
		&i.Price,
		&i.Rating,
		&i.VoteCount,
		&i.VoteSum,
	)
	return i, err
}

const updateProductVote = `-- name: UpdateProductVote :one
UPDATE products SET
    vote_count = $2,
    vote_sum = $3,
    rating = $4
WHERE id = $1
RETURNING id, name, article, brand, country_of_origin, description, price, rating, vote_count, vote_sum
`

type UpdateProductVoteParams struct {
	ID        int32  `json:"id"`
	VoteCount int32  `json:"vote_count"`
	VoteSum   int32  `json:"vote_sum"`
	Rating    string `json:"rating"`
}

func (q *Queries) UpdateProductVote(ctx context.Context, arg UpdateProductVoteParams) (Product, error) {
	row := q.db.QueryRowContext(ctx, updateProductVote,
		arg.ID,
		arg.VoteCount,
		arg.VoteSum,
		arg.Rating,
	)
	var i Product
	err := row.Scan(
		&i.ID,
		&i.Name,
		&i.Article,
		&i.Brand,
		&i.CountryOfOrigin,
		&i.Description,
		&i.Price,
		&i.Rating,
		&i.VoteCount,
		&i.VoteSum,
	)
	return i, err
}
