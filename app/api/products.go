package api

import (
	"database/sql"
	"education-golang/app/api/parameters"
	"education-golang/app/api/responses"
	"education-golang/app/processors"
	"github.com/gorilla/mux"
	"net/http"
)

type Products struct {
	processors *processors.Processors
}

func NewProductsApi(processors *processors.Processors) *Products {
	return &Products{
		processors: processors,
	}
}

func (p *Products) HandleMethods(router *mux.Router) {
	router.HandleFunc("/products/{product_id:[0-9]+}", p.Get()).Methods("GET")
	//router.HandleFunc("/products", p.List()).Methods("GET")
}

//func (p *Products) List() http.HandlerFunc {
//	type listParams struct {
//		Page string `json:"page"`
//		Limit string `json:"limit"`
//	}
//
//	return func(writer http.ResponseWriter, request *http.Request) {
//
//
//		io.WriteString(writer, "Products list")
//	}
//}

func (p *Products) Get() http.HandlerFunc {
	return func(responseWriter http.ResponseWriter, request *http.Request) {
		productsGetParams, err := parameters.NewProductsGetParamsFromRequest(request)
		if err != nil {
			response := responses.NewErrorResponse(http.StatusBadRequest, err.Error())
			response.WriteErrorResponse(responseWriter)
		}

		product, err := p.processors.ProductProcessor.Get(request.Context(), productsGetParams.ProductID)
		if err != nil {
			response := responses.ErrorResponse{}
			switch err {
			case sql.ErrNoRows:
				response = responses.NewErrorResponse(http.StatusNotFound, "Product not found")
			default:
				response = responses.NewErrorResponse(http.StatusInternalServerError, err.Error())
			}
			response.WriteErrorResponse(responseWriter)
			return
		}

		productOkResponse := responses.NewProductGetOkResponse(product)
		productOkResponse.WriteResponse(responseWriter)
	}
}
