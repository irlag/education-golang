package parameters

type ProductsListParams struct {
	Fields []string `query:"fields"`
	Limit  int32    `query:"limit"`
	Page   int32    `query:"page"`
	SortBy string   `query:"sort_by"`
}
