package parameters

import (
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

type ProductsGetParams struct {
	Fields    []string `query:"fields"`
	ProductID int32    `query:"product_id"`
}

func NewProductsGetParamsFromRequest(request *http.Request) (*ProductsGetParams, error) {
	vars := mux.Vars(request)
	queryParams := request.URL.Query()

	productId, err := strconv.Atoi(vars["product_id"])
	if err != nil {
		return nil, err
	}

	return &ProductsGetParams{
		Fields:    queryParams["fields"],
		ProductID: int32(productId),
	}, nil
}
