package responses

import (
	"net/http"
)

type ErrorResponse struct {
	Code    int
	Message string
}

func NewErrorResponse(code int, message string) ErrorResponse {
	return ErrorResponse{
		Code:    code,
		Message: message,
	}
}

func (e *ErrorResponse) WriteErrorResponse(rw http.ResponseWriter) {
	WriteJsonResponse(rw, e.Code, e)
}
