package responses

import (
	db "education-golang/app/storage/db/sqlc"
	"net/http"
)

type ProductGetOkResponse struct {
	ID              int32  `json:"id"`
	Name            string `json:"name"`
	Article         string `json:"article"`
	Brand           string `json:"brand"`
	CountryOfOrigin string `json:"country_of_origin"`
	Description     string `json:"description"`
	Price           string `json:"price"`
	Rating          string `json:"rating"`
}

func NewProductGetOkResponse(product db.Product) ProductGetOkResponse {
	return ProductGetOkResponse{
		ID:              product.ID,
		Name:            product.Name,
		Article:         product.Article,
		Brand:           product.Brand,
		CountryOfOrigin: product.CountryOfOrigin,
		Description:     product.Description.String,
		Price:           product.Price,
		Rating:          product.Rating,
	}
}

func (p *ProductGetOkResponse) WriteResponse(rw http.ResponseWriter) {
	WriteJsonResponse(rw, http.StatusOK, p)
}
