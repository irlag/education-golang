package responses

import (
	"encoding/json"
	"net/http"
	"strconv"
)

func WriteJsonResponse(rw http.ResponseWriter, code int, payload interface{}) {
	data, _ := json.Marshal(payload)

	rw.Header().Set("Content-Type", "application/json; charset=utf-8")
	rw.Header().Set("Content-Length", strconv.Itoa(len(data)))

	rw.WriteHeader(code)

	rw.Write(data)
}
