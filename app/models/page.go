package models

type Page struct {
	page   int32
	limit  int32
	offset int32
}
