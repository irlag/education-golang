package processors

import (
	"context"
	db "education-golang/app/storage/db/sqlc"
)

type ProductProcessor interface {
	Get(ctx context.Context, id int32) (product db.Product, err error)
}

type productProcessor struct {
	store db.Store
}

func NewProductProcessor(store db.Store) ProductProcessor {
	return &productProcessor{
		store: store,
	}
}

func (p *productProcessor) Get(ctx context.Context, id int32) (product db.Product, err error) {
	product, err = p.store.GetProduct(ctx, id)
	if err != nil {
		return product, err
	}

	return product, nil
}
