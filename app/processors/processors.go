package processors

import (
	db "education-golang/app/storage/db/sqlc"
	"go.uber.org/zap"
)

type Processors struct {
	ProductProcessor ProductProcessor
}

func NewProcessor(
	store db.Store,
	log *zap.Logger,
) *Processors {
	return &Processors{
		ProductProcessor: NewProductProcessor(store),
	}
}
