include .env
export

.PHONY: build
build:
	docker-compose run --rm app go build -v -o bin/apiserver ./main.go

.PHONY: sh
sh:
	docker-compose exec app bash

.PHONY: run
run:
	docker-compose run --service-ports --rm app

PHONY: stop
stop:
	docker-compose stop

PHONY: down
down:
	docker-compose down --remove-orphans

.PHONY: remove
remove:
	docker-compose down -v --rmi all

.PHONY: clean
clean:
	go clean

.PHONY: migrations-create
migrations-create:
ifdef name
	docker-compose run --rm app-migrate -database=${DB_DSN} create -ext sql $(name)
else
	@echo 'Error. Set migration name'
endif

.PHONY: migrations-up
migrations-up:
	docker-compose run --rm app-migrate -path=/migrations/ -database=${DB_DSN} up

.PHONY: sqlc-generate
sqlc-generate:
	docker-compose run --rm app-sqlc generate


.DEFAULT_GOAL := build
