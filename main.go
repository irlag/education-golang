package main

import (
	"education-golang/cmd"
)

func main() {
	cmd.Execute()
}
