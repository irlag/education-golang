module education-golang

go 1.16

require (
	contrib.go.opencensus.io/exporter/stackdriver v0.6.0 // indirect
	git.apache.org/thrift.git v0.0.0-20180924222215-a9235805469b // indirect
	github.com/Nvveen/Gotty v0.0.0-20120604004816-cd527374f1e5 // indirect
	github.com/cznic/ql v1.2.0 // indirect
	github.com/go-ini/ini v1.39.0 // indirect
	github.com/golang-migrate/migrate/v4 v4.14.1 // indirect
	github.com/golang/lint v0.0.0-20180702182130-06c8688daad7 // indirect
	github.com/googleapis/gax-go v2.0.0+incompatible // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/jackc/pgx v3.2.0+incompatible // indirect
	github.com/kshvakov/clickhouse v1.3.4 // indirect
	github.com/lib/pq v1.10.1 // indirect
	github.com/mongodb/mongo-go-driver v0.1.0 // indirect
	github.com/openzipkin/zipkin-go v0.1.1 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.1.3
	github.com/vrischmann/envconfig v1.3.0
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.16.0
)
